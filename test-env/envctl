#!/bin/bash
# QEMU/VDE network environment preparation script
RUN='/run/vde'
qemu=/opt/qemu/build/qemu-system-x86_64
proxy=`dirname "$(readlink -f "$0")"`/../proxy/proxy

# PID identifiers
all="$RUN/*.pid"
network="$RUN/net_*.pid"
vms="$RUN/vm_*.pid"
alpine="$RUN/vm_alpine_*.pid"
alpine1="$RUN/vm_alpine_1.pid"
alpine2="$RUN/vm_alpine_2.pid"
win="$RUN/vm_win_*.pid"
win1="$RUN/vm_win_1.pid"
win2="$RUN/vm_win_2.pid"
kali1="$RUN/vm_kali1.pid"
kali2="$RUN/vm_kali2.pid"
proxy2="$RUN/proxy_2.pid"
proxy3="$RUN/proxy_3.pid"
proxies="$RUN/proxy_*.pid"


case "$1" in
start)
  sudo mkdir -p $RUN && sudo chown "$(id -un)":"$(id -gn)" $RUN
  case "$2" in
  network | all)
    echo "Starting VDE network for QEMU: "
    vde_switch -x -daemon -s $RUN/sw_main.sock -p $RUN/net_sw_main.pid
    vde_switch -x -daemon -s $RUN/sw_proxy1.sock -p $RUN/net_sw_proxy1.pid
    vde_switch -x -daemon -s $RUN/sw_proxy2.sock -p $RUN/net_sw_proxy2.pid
    vde_switch -x -daemon -s $RUN/sw_proxy3.sock -p $RUN/net_sw_proxy3.pid
    slirpvde -D -H 10.0.0.2 --daemon -s $RUN/sw_main.sock -p $RUN/net_slirp.pid
    ;;&
  alpine1 | alpine)
    $qemu -m 512 -nic vde,mac='52:54:00:12:34:56',sock=$RUN/sw_proxy1.sock -hda alpine1.qcow2 -daemonize -vnc :1 -pidfile $alpine1
    ;;&
  alpine1b | alpine)
    $qemu -m 512 -nic vde,mac='52:54:00:12:34:56',sock=$RUN/sw_proxy2.sock -hda alpine1b.qcow2 -daemonize -vnc :12 -pidfile $alpine2
    ;;&
  alpine2 )
    $qemu -m 512 -nic vde,mac='52:54:00:12:34:66',sock=$RUN/sw_proxy2.sock -hda alpine2.qcow2 -daemonize -vnc :2 -pidfile $alpine2
    ;;&
  win1 | win | vms )
    $qemu -vga cirrus -smp 1 -net nic,model=rtl8139 -net vde,sock=$RUN/sw_proxy1.sock -soundhw sb16 -m 128 -usb -usbdevice tablet -drive file=images/win98.raw,format=raw,index=0,media=disk -cdrom images/Win98SE.iso -daemonize -vnc :1 -pidfile $win1
    ;;&
  win0 | win | vms )
    $qemu -vga cirrus -smp 1 -net nic,model=rtl8139,macaddr='52:54:00:12:34:66' -net vde,sock=$RUN/sw_proxy1.sock -soundhw sb16 -m 128 -usb -usbdevice tablet -drive file=images/win98-0.raw,format=raw,index=0,media=disk -cdrom images/Win98SE.iso -daemonize -vnc :1 -pidfile $win1
    ;;&
  win2 | win | vms )
    $qemu -vga cirrus -smp 1 -net nic,model=rtl8139,macaddr='52:54:00:12:34:66' -net vde,sock=$RUN/sw_proxy2.sock -soundhw sb16 -m 128 -usb -usbdevice tablet -drive file=images/win98-2.raw,format=raw,index=0,media=disk -cdrom images/Win98SE.iso -daemonize -vnc :2 -pidfile $win2
    ;;&
  kali1 | kalis | vms | all)
    $qemu -m 1024 -nic user -nic vde,mac='52:54:00:12:34:76',sock=$RUN/sw_proxy1.sock -hda kali1.qcow2 -daemonize -vnc :1 -pidfile $kali1
    ;;&
  kali2 | kalis | vms | all)
    $qemu -m 1024 -nic user -nic vde,mac='52:54:00:12:34:76',sock=$RUN/sw_proxy2.sock -hda kali2.qcow2 -daemonize -vnc :2 -pidfile $kali2
    ;;&
  proxy2 | proxies | all)
    $proxy -sproxy $RUN/sw_proxy2.sock -passthrough -logfile $RUN/proxy_2.log -pidfile $proxy2 &
    ;;&
  proxy3 | proxies )
    $proxy -sproxy $RUN/sw_proxy3.sock -passthrough -logfile $RUN/proxy_3.log -pidfile $proxy3 &
    ;;
  *)
    echo "Usage: envctl start {all|network|vms|alpine|alpine1|alpine2|win1|win2|kali|proxy2|proxy3|proxies}"
  esac
  #echo "Run:\nqemu -m 512 -nic vde,mac='52:54:00:12:34:56',sock=$RUN/sw_proxy.sock -hda alpine1.qcow2 -nographic"
  ;;
stop)
  if [ -n "${!2}" ]; then
    if ls ${!2} 2> /dev/null; then
      cat ${!2} | xargs kill
      rm -f ${!2}
    else
      echo "$2 is not running"
    fi
  else
    echo "Usage: envctl stop {all|network|vms|alpine|alpine1|alpine2|win1|win2|kali|proxy2|proxy3|proxies}"
  fi
  ;;
restart)
  $0 stop
  sleep 1
  $0 start
  ;;
status)
  if [ -n "${!2}" ]; then
    if ls ${!2} 2> /dev/null; then
      ps -fq "$(cat ${!2} | xargs | sed 's/ /,/g')"
    else
      echo "$2 is not running"
    fi
  else
    echo "Usage: envctl status {all|network|vms|alpine|alpine1|alpine2|win1|win2|kali|proxy2|proxy3|proxies}"
  fi
  ;;
*)
  echo "Usage: envctl {start|stop|restart|status} {all|network|vms|alpine|alpine1|alpine2|win1|win2|kali|proxy2|proxy3|proxies}"
  exit 1
  ;;
esac
exit 0
