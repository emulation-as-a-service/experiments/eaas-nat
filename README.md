# eaas-vde-proxy

## Overview

![overview](README.assets/overview.png)

## Documentation
See [Wiki](../../../wiki/Home)

## Problematic protocols

Important and implemented:

* DHCP
* ARP
* NBNS/NBDS (by filtering binary payload)

Not supported:

* IPv6 (NDP, Interface Identifier, ...)
* IPSec
* 802.1X
* IPX/LLC

## Third party libraries

* [gopacket](https://github.com/google/gopacket): similar to libpcap, for parsing and serializing packets *(BSD 3-clause license, authors: Andreas Krennmair & Google)*
* [dhcp4](https://github.com/krolaw/dhcp4): for handling and sending DHCP requests/responses *(BSD 3-clause license, author: Richard Warburton/krolaw)*
* [logrus](https://github.com/sirupsen/logrus): improved logging *(MIT license, author: Simon Eskildsen/sirupsen)*

## Binaries
### proxy - main binary
```
$ ./proxy -help
Usage of ./proxy:
  -hostname string
        Set a windows hostname to filter for in binary payloads
  -log int
        allowed: 5 (debug), 4 (info), 3 (warning), 2 (error), 1 (fatal) (default 4)
  -logfile string
        Location to write output to (optional)
  -newip string
        Force IP after change (optional)
  -newmac string
        Force MAC after change (optional)
  -oldip string
        Force IP before change (optional)
  -oldmac string
        Force MAC before change (optional)
  -passthrough
        Whether to pass every traffic through
  -pidfile string
        Location to write the pid to (optional)
  -smain string
        Main switch sock path, - for stdin/out (default "/run/vde/sw_main.sock")
  -sproxy string
        Proxy switch sock path (default "/run/vde/sw_proxy1.sock")
  -wireshark
        Whether to write all traffic to /tmp
```

### envctl - control VMs, network etc
```
$ ./envctl
Usage: envctl {start|stop|restart|status} {all|network|vms|alpine|alpine1|alpine2|win1|win2|kali|proxy2|proxy3|proxies}
```

## Mirrors
- https://naclador.de/mosers/eaas-vde-proxy
- https://codeberg.org/MrMcX/eaas-vde-proxy