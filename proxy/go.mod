module proxy

go 1.13

require (
	github.com/google/gopacket v1.1.19
	github.com/krolaw/dhcp4 v0.0.0-20190909130307-a50d88189771
	github.com/sirupsen/logrus v1.8.1
)
